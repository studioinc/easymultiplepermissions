package com.medstudioinc.permissions;

import android.content.Intent;

public interface ActivityResultCallback {
    void startSettingsForResult(Intent intent, int request);
    void onAppSettingsCanceled();
}
