package com.medstudioinc.permissions;

import java.util.ArrayList;

public interface PermissionsCallback {
    void onPermissionGranted(int requestCode, String[] permissions);
    void onPartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions);
    void onPermissionDenied(int requestCode, String[] permissions);
    void onNeverAskAgain(int requestCode, boolean isAgain);
    void onRemoveItemFromList(int requestCode, String[] permissions);
}
