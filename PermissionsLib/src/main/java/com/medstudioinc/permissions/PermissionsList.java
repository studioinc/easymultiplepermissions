package com.medstudioinc.permissions;

public class PermissionsList {

    public static final String HTC_READ_SETTINGS = "com.htc.launcher.permission.READ_SETTINGS";
    public static final String HTC_UPDATE_SHORTCUT = "com.htc.launcher.permission.UPDATE_SHORTCUT";

    public static final String SONY_ERICSSON_BROADCAST_BADGE = "com.sonyericsson.home.permission.BROADCAST_BADGE";
    public static final String SONY_MOBILE_BROADCAST_BADGE = "com.sonymobile.home.permission.PROVIDER_INSERT_BADGE";

    public static final String HUAWEI_CHANGE_BADGE = "com.huawei.android.launcher.permission.CHANGE_BADGE";
    public static final String HUAWEI_READ_SETTINGS = "com.huawei.android.launcher.permission.READ_SETTINGS";
    public static final String HUAWEI_WRITE_SETTINGS = "com.huawei.android.launcher.permission.WRITE_SETTINGS";

    public static final String OPPO_READ_SETTINGS = "com.oppo.launcher.permission.READ_SETTINGS";
    public static final String OPPO_WRITE_SETTINGS = "com.oppo.launcher.permission.WRITE_SETTINGS";

}
