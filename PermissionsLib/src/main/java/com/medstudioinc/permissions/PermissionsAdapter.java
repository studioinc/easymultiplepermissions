package com.medstudioinc.permissions;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medmobilelab.permissionssdk.R;

import java.util.ArrayList;
import java.util.Iterator;

import hakobastvatsatryan.DropdownTextView;


public class PermissionsAdapter extends RecyclerView.Adapter<PermissionsAdapter.MyHolder>{
    public ArrayList<PermissionsModel> permissionsModelArrayList;
    private String[] permissions = null;

    public void addPermissionsList(ArrayList<PermissionsModel> stringArrayList) {
        this.permissionsModelArrayList = stringArrayList;
    }


    class MyHolder extends RecyclerView.ViewHolder {
        DropdownTextView dropdownTextView;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            dropdownTextView = (DropdownTextView)itemView.findViewById(R.id.dropdown_text_view);
        }
    }



    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dropdown_expandable, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, int i) {
        PermissionsModel permissionsModel = permissionsModelArrayList.get(i);
        myHolder.dropdownTextView.setTitleText(permissionsModel.getDropDownTitle());
        myHolder.dropdownTextView.setContentText(permissionsModel.getDropDownContent());
    }


    public String[] getPermissions(){
        permissions = new String[permissionsModelArrayList.size()];
        for (int i = 0; i < permissions.length; i++){
            permissions[i] = permissionsModelArrayList.get(i).getPermissions();
        }
        return permissions;
    }



    public void removeItems(String item){
        Iterator itr = permissionsModelArrayList.iterator();
        PermissionsModel strElement;
        while (itr.hasNext()) {
            strElement = (PermissionsModel) itr.next();
            if (strElement.getPermissions().equals(item)) {
                itr.remove();
                notifyDataSetChanged();
            }
        }

    }

    public void removeItem(String item){
        for(PermissionsModel str : permissionsModelArrayList) {
            if (str.getPermissions().equals(item)){
                permissionsModelArrayList.remove(item);
                notifyDataSetChanged();
            }
        }

    }


    @Override
    public int getItemCount() {
        return permissionsModelArrayList!= null ? permissionsModelArrayList.size() : 0;
    }

    public boolean isEmpty(){
        return getItemCount() <= 0;
    }
}
