package com.medstudioinc.permissions;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.medmobilelab.permissionssdk.R;

import java.util.ArrayList;

public class EasyPermissionsActivity extends AppCompatActivity implements PermissionsCallback,ActivityResultCallback {
    private TextView permissionConfirm;
    private RecyclerView recyclerView;
    private PermissionsAdapter permissionsAdapter;
    private PermissionsManager permissionsManager;
    private TextView topBarTitle;
    private TextView topBarSubTitle;
    private TextView permsGranted;
    private ArrayList<PermissionsModel> permissionsModelArrayList = new ArrayList<>();

    


    @Override
    public void startSettingsForResult(Intent intent, int request) {
        startActivityForResult(intent, request);
    }

    @Override
    public void onAppSettingsCanceled() {
        this.checkingPerms();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.medmobilelab.permissionssdk.R.layout.easy_permissions_activity);

        this.topBarTitle = (TextView) findViewById(com.medmobilelab.permissionssdk.R.id.txt_top_bar_title);
        this.topBarSubTitle = (TextView) findViewById(com.medmobilelab.permissionssdk.R.id.txt_top_bar_subtitle);
        this.permsGranted = (TextView) findViewById(com.medmobilelab.permissionssdk.R.id.txt_permissions_msg);

        this.recyclerView = (RecyclerView) findViewById(com.medmobilelab.permissionssdk.R.id.recycler_view);
        this.permissionConfirm = (TextView) findViewById(com.medmobilelab.permissionssdk.R.id.permissionConfirm);


        if (getIntent() != null && getIntent().getExtras() != null) {
            this.permissionsModelArrayList = (ArrayList<PermissionsModel>) getIntent().getSerializableExtra(PermissionsConstants.PERMISSION_LIST);
        }

        this.permissionsManager = PermissionsManager.getInstance(this);
        this.permissionsManager.addArrayListItem(this.permissionsModelArrayList);
        this.permissionsManager.listerner(this);
        this.permissionsManager.addActivityResultCallback(this);

        //this.permissionsManager = InitPermissions.getInstance(this);
        //permissionsManager = PermissionsManager.with(this).addPermissions(permissions).listerner(this);
        //permissionsManager = PermissionsManager.getInstance(this).addPermissions(permissions).listerner(this);
        //permissionsManager = new PermissionsManager(this);
        //permissionsManager = PermissionsManager.getInstance(this);
        //permissionsManager.listerner(this);
        //permissionsManager.addPermissions(permissions);


        this.permissionsAdapter = new PermissionsAdapter();
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.setAdapter(this.permissionsAdapter);

        this.topBarTitle.setText("Permissions Required");
        this.topBarSubTitle.setText(getResources().getString(com.medmobilelab.permissionssdk.R.string.app_name) + " needs to accees to:");

        this.addPermissionsItem();
        this.permissionsManager.checkSelfGranted();
        this.checkingPerms();

        this.permissionConfirm.setOnClickListener(e-> {
            this.permissionsManager.checkAndRequestPermissions(this);
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.permissionsManager.onActivityResult(requestCode,resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        this.permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void addPermissionsItem(){
        this.permissionsAdapter.addPermissionsList(this.permissionsManager.getPermissionsModelArrayList());
        this.permissionsAdapter.notifyDataSetChanged();
    }

    private void checkingPerms(){
        if (this.permissionsManager.checkPermissions() ){
            this.recyclerView.setVisibility(View.GONE);
            this.permsGranted.setVisibility(View.VISIBLE);
            CharSequence htmlText = Html.fromHtml("<div style=\"font-size:15px\">" + TextUtils.join("\n", permissionsManager.permissions) + "</div>");
            this.permsGranted.setText("All permissions granted! \n\n" + htmlText);
            new Handler().postDelayed(() -> {
                this.permissionsManager.startNextActivity();

            }, 500);
        }
    }

    @Override
    public void onPermissionGranted(int requestCode, String[] permissions) {
        this.checkingPerms();
    }

    @Override
    public void onPartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions) {

    }

    @Override
    public void onPermissionDenied(int requestCode, String[] permissions) {
        this.permissionsManager.checkSelfGranted();
        Toast.makeText(this, "All permissions denied", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNeverAskAgain(int requestCode, boolean isAgain) {
        this.permissionsManager.addSharedPrefNeverAskAgain(isAgain);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(com.medmobilelab.permissionssdk.R.string.permission_required)
                .setMessage("Don't ask again")
                .setNegativeButton(android.R.string.ok, (dialog, id) -> dialog.cancel())
                .setPositiveButton("Settings", (dialog, id) -> this.permissionsManager.openAppSettings());

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRemoveItemFromList(int requestCode, String[] permissions) {
        if (this.permissionsAdapter.isEmpty()){
            this.checkingPerms();
            return;
        }
        for (String perms : permissions){
            this.permissionsAdapter.removeItems(perms);
        }
    }
}
