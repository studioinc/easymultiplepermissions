package com.medstudioinc.permissions;

public interface RequestPermissionListener {
    void onSuccess();

    void onFailed();
}