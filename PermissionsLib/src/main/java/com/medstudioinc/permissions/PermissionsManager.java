package com.medstudioinc.permissions;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.medmobilelab.permissionssdk.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PermissionsManager<T> {
    public static String TAG = PermissionsManager.class.getName();
    private static PermissionsManager permissionsManager;
    private static Activity mActivity;
    private SharedPreferences preferences;
    private String pref_key = "never_ask_again_key";
    private String pref_name = "never_ask_again_name";

    // list of permissions
    public String[] permissions;

    public PermissionsCallback mPermissionsCallback;
    public ActivityResultCallback mActivityResultCallback;

    public PermissionsModel permissionsModel = new PermissionsModel();
    public ArrayList<PermissionsModel> permissionsModelArrayList;

    public static synchronized PermissionsManager getInstance(Activity activity){
        if (permissionsManager == null){
            return new PermissionsManager(activity);
        }
        return permissionsManager;
    }

    public static PermissionsManager with(Activity activity){
        return new PermissionsManager(activity);
    }

    public PermissionsManager(Activity activity) {
        this.mActivity = activity;
        this.preferences = initSharedPreferences();
    }

    public PermissionsManager(Activity activity, String... permissions) {
        this.mActivity = activity;
        this.permissions = permissions;
    }

    public PermissionsManager listerner(PermissionsCallback resultCallback){
        this.mPermissionsCallback = resultCallback;
        return this;
    }

    public void addActivityResultCallback(ActivityResultCallback callback){
        this.mActivityResultCallback = callback;
    }

    public PermissionsManager addPermissions(String[] permissions) {
        this.permissions = permissions;
        return this;
    }

    public PermissionsManager addArrayListItem(ArrayList<PermissionsModel> models) {
        this.permissionsModelArrayList = models;
        this.permissions = new String[models.size()];
        for (int i = 0; i < models.size(); i++){
            this.permissionsModel = permissionsModelArrayList.get(i);
            this.permissions[i] = this.permissionsModel.getPermissions();
        }
        return this;
    }

    public ArrayList<PermissionsModel> getPermissionsModelArrayList(){
        return permissionsModelArrayList;
    }

    public void startNextActivity(){
        Intent intent = new Intent();
        this.mActivity.setResult(PermissionsConstants.INTENT_CODE, intent);
        this.mActivity.finish();
    }

    public void requestPermissions(T val){
        if (isSharedPrefNeverAskAgain()) {
            openAppSettings();
        } else {
            if(!hasPermissions(this.mActivity)){
                if (val instanceof Fragment){
                    Fragment fragment = (Fragment) val;
                    fragment.requestPermissions(this.permissions, PermissionsConstants.REQUEST_PERMISSION_ALL);
                    return;
                }
                ActivityCompat.requestPermissions(this.mActivity, this.permissions, PermissionsConstants.REQUEST_PERMISSION_ALL);
            } else {
                if (this.mPermissionsCallback != null){
                    this.mPermissionsCallback.onPermissionGranted(PermissionsConstants.REQUEST_PERMISSION_ALL, null);
                }
            }
        }
    }

    public void checkSelfGranted(){
        List<String> listPermissionsUnNeeded = new ArrayList<>();
        for (String permission : this.permissions) {
            if (ContextCompat.checkSelfPermission(this.mActivity, permission) == PackageManager.PERMISSION_GRANTED) {
                listPermissionsUnNeeded.add(permission);
            }
        }
        if (this.mPermissionsCallback != null){
            this.mPermissionsCallback.onRemoveItemFromList(PermissionsConstants.REQUEST_PERMISSION_ALL, listPermissionsUnNeeded.toArray(new String[listPermissionsUnNeeded.size()]));
        }
    }

    public void checkAndRequestPermissions(T val) {
        if (this.isSharedPrefNeverAskAgain()) {
            this.openAppSettings();
        } else {
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (String permission : this.permissions) {
                if (ContextCompat.checkSelfPermission(this.mActivity, permission) != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permission);
                }
            }


            List<String> listPermissionsUnNeeded = new ArrayList<>();
            for (String permission : this.permissions) {
                if (ContextCompat.checkSelfPermission(this.mActivity, permission) == PackageManager.PERMISSION_GRANTED) {
                    listPermissionsUnNeeded.add(permission);
                }
            }

            if (this.mPermissionsCallback != null){
                this.mPermissionsCallback.onRemoveItemFromList(PermissionsConstants.REQUEST_PERMISSION_ALL, listPermissionsUnNeeded.toArray(new String[listPermissionsUnNeeded.size()]));
            }
            Log.d("listPermissionsUnNeeded", listPermissionsUnNeeded.size()+"");

            if (!listPermissionsNeeded.isEmpty()) {
                if (val instanceof Fragment){
                    Fragment fragment = (Fragment) val;
                    fragment.requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PermissionsConstants.REQUEST_PERMISSION_ALL);
                } else {
                    ActivityCompat.requestPermissions(this.mActivity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PermissionsConstants.REQUEST_PERMISSION_ALL);
                }
            } else {
                if (this.mPermissionsCallback != null){
                    this.mPermissionsCallback.onPermissionGranted(PermissionsConstants.REQUEST_PERMISSION_ALL, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]));
                }
            }
        }
    }

    public boolean checkPermissions(){
        boolean isGranted;
        if(!hasPermissions(this.mActivity)){
            isGranted = false;
        } else {
            isGranted = true;
        }
        return isGranted;
    }

    public boolean hasPermissions(Context context) {
        if (needRequestRuntimePermissions()){
            if (context != null && this.permissions != null) {
                for (String permission : this.permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    }
                }
            }
            return true;
        }
        return true;
    }

    private boolean needRequestRuntimePermissions() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public void requestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
            boolean permsDenied = false;
            boolean permsGranted = false;
            boolean permsDontAskAgain = false;
            for (int i = 0; i < permissions.length; i++){
                String listPerms = permissions[i];
               if (grantResults[i] > 0) {
                   //allowed
                   permsGranted = true;
                } else {
                   if (ActivityCompat.shouldShowRequestPermissionRationale(this.mActivity, listPerms)) {
                       //denied
                       permsDenied = true;
                   } else {
                       if (ActivityCompat.checkSelfPermission(this.mActivity, listPerms) == PackageManager.PERMISSION_GRANTED) {
                           //allowed
                           permsGranted = true;
                       } else {
                           // Don't ask again
                           permsDontAskAgain = true;
                       }
                   }
               }
            }

            if (permsDenied){
                if (this.mPermissionsCallback != null){
                    this.mPermissionsCallback.onPermissionDenied(requestCode, permissions);
                }
            }
            if (permsGranted){
                if (this.mPermissionsCallback != null){
                    this.mPermissionsCallback.onPermissionGranted(grantResults.length, permissions);
                }
            }
            if (permsDontAskAgain){
                if (this.mPermissionsCallback != null){
                    this.mPermissionsCallback.onNeverAskAgain(requestCode, true);
                }
            }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode){
            case PermissionsConstants.REQUEST_PERMISSION_ALL:{
                this.requestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("resultType", data+"");
        switch (requestCode){
            case PermissionsConstants.ACTIVITY_RESULT_REQUEST:{
                if (resultCode == Activity.RESULT_CANCELED){
                    if (this.mActivityResultCallback != null){
                        this.mActivityResultCallback.onAppSettingsCanceled();
                    }

                }
            }
        }
    }

    public SharedPreferences initSharedPreferences(){
        return this.mActivity.getSharedPreferences(this.pref_name, Context.MODE_PRIVATE);
    }

    public void addSharedPrefNeverAskAgain(boolean isNeverAsk){
        this.preferences.edit().putBoolean(this.pref_key, isNeverAsk).apply();
    }

    public boolean isSharedPrefNeverAskAgain(){
        return this.preferences.getBoolean(this.pref_key, false);
    }

    public void openAppSettings(){
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", this.mActivity.getPackageName(), null);
            intent.setData(uri);
            if (this.mActivityResultCallback != null){
                this.mActivityResultCallback.startSettingsForResult(intent, PermissionsConstants.ACTIVITY_RESULT_REQUEST);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public Uri setFileProvider(File file){
        return FileProvider.getUriForFile(this.mActivity, this.mActivity.getPackageName() + ".provider", file);
    }

    public int size(){
        return this.permissions.length;
    }

    public void clearCache() {
        try {
            if (deleteDir(this.mActivity.getCacheDir())) {
                Toast.makeText(this.mActivity, this.mActivity.getResources().getString(R.string.clearedSuccess), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this.mActivity, this.mActivity.getResources().getString(R.string.anErrorOccured), Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir == null || !dir.isFile()) {
            return false;
        } else {
            return dir.delete();
        }
    }


    public static Intent doRestart(Context c) {
        Intent mStartActivity;
        try {
            //check if the context is given
            if (c != null) {
                //fetch the packagemanager so we can get the default launch activity
                // (you can replace this intent with any other activity if you want
                PackageManager pm = c.getPackageManager();
                //check if we got the PackageManager
                if (pm != null) {
                    //create the intent with the default start activity for your application
                    mStartActivity = pm.getLaunchIntentForPackage(c.getPackageName()
                    );
                    if (mStartActivity != null) {
                        mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //create a pending intent so the application is restarted after System.exit(0) was called.
                        // We use an AlarmManager to call this intent in 100ms
                        int mPendingIntentId = 223344;
                        PendingIntent mPendingIntent = PendingIntent.getActivity(c, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        //kill the application
                        System.exit(0);
                        return mStartActivity;
                    } else {
                        Log.e(TAG, "Was not able to restart application, mStartActivity null");
                    }
                } else {
                    Log.e(TAG, "Was not able to restart application, PM null");
                }
            } else {
                Log.e(TAG, "Was not able to restart application, Context null");
            }
        } catch (Exception ex) {
            Log.e(TAG, "Was not able to restart application");
        }

        return null;
    }

    // Builder class
    public static class Builder {
        private Activity mActivity;
        private PermissionsManager mPermissionsManager;
        private ArrayList<PermissionsModel> modelArrayList;
        private PermissionsCallback mCallback;

        public Builder addContext(Activity activity) {
            this.mActivity = activity;
            return this;
        }

        public Builder addArrayListItem(ArrayList<PermissionsModel> models) {
            this.modelArrayList = models;
            return this;
        }


        public Builder addListener(PermissionsCallback resultCallback) {
            this.mCallback = resultCallback;
            return this;
        }

        public PermissionsManager build(){
            this.mPermissionsManager = new PermissionsManager(this.mActivity);
            this.mPermissionsManager.addArrayListItem(this.modelArrayList);
            this.mPermissionsManager.listerner(this.mCallback);
            return this.mPermissionsManager;
        }
    }
}
