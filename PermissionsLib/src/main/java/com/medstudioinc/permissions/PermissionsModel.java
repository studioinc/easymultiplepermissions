package com.medstudioinc.permissions;

import java.io.Serializable;

public class PermissionsModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private String dropDownTitle;
    private String dropDownContent;
    private String permissions;

    public PermissionsModel() {
    }

    public PermissionsModel(String dropDownTitle, String dropDownContent, String permissions) {
        this.dropDownTitle = dropDownTitle;
        this.dropDownContent = dropDownContent;
        this.permissions = permissions;
    }


    public PermissionsModel(String permissions) {
        this.permissions = permissions;
    }

    public String getDropDownTitle() {
        return dropDownTitle;
    }

    public void setDropDownTitle(String dropDownTitle) {
        this.dropDownTitle = dropDownTitle;
    }

    public String getDropDownContent() {
        return dropDownContent;
    }

    public void setDropDownContent(String dropDownContent) {
        this.dropDownContent = dropDownContent;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
