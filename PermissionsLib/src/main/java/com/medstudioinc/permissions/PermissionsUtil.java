package com.medstudioinc.permissions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import java.io.Serializable;
import java.util.ArrayList;

public class PermissionsUtil extends Intent {

    public PermissionsUtil(Activity context, ArrayList<PermissionsModel> permissionsModels) {
        Intent intent = new Intent();
        intent.setClass(context, EasyPermissionsActivity.class);
        intent.putExtra(PermissionsConstants.PERMISSION_LIST, (Serializable) permissionsModels);
        context.startActivityForResult(intent, PermissionsConstants.INTENT_CODE);
    }

    public static PermissionsUtil with(Activity ctx, ArrayList<PermissionsModel> modelArrayList){
        return new PermissionsUtil(ctx, modelArrayList);
    }
}
