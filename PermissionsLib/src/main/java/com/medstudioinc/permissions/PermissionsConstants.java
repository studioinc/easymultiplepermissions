package com.medstudioinc.permissions;

public class PermissionsConstants {
    public static final int ACTIVITY_RESULT_REQUEST = 20; // code you want.
    public static final int REQUEST_PERMISSION_ALL = 101;
    public static final int INTENT_CODE = 001;

    public static final String PERMISSION_LIST = "PERMISSION_LIST";
    public static final String DENIED_PERMISSION_LIST = "DENIED_PERMISSION_LIST";

    public static final String IS_GOT_ALL_PERMISSION = "IS_GOT_ALL_PERMISSION";
}
