package com.medstudioinc.permissions;

import android.Manifest;
import android.app.Activity;

import java.util.ArrayList;

public class InitPermissions {

    public static ArrayList<PermissionsModel> getList(){
        ArrayList<PermissionsModel> modelArrayList = new ArrayList<>();
        modelArrayList.add(new PermissionsModel("Storage", "To save videos in external storage or SD Card", Manifest.permission.WRITE_EXTERNAL_STORAGE));
        modelArrayList.add(new PermissionsModel("Location", "To check your location country", Manifest.permission.ACCESS_FINE_LOCATION));
        modelArrayList.add(new PermissionsModel("Camera", "To take picture and video", Manifest.permission.CAMERA));
        return modelArrayList;
    }

    // get instance in activity with callback
    public static PermissionsManager getInstance(Activity activity){
        return new PermissionsManager.Builder()
                .addContext(activity)
                .addArrayListItem(getList())
                .build();
    }

    // get instance in activity with callback
    public static PermissionsManager getInstance(Activity activity, PermissionsCallback callback){
        return new PermissionsManager.Builder()
                .addContext(activity)
                .addArrayListItem(getList())
                .addListener(callback)
                .build();
    }
}