package com.medstudioinc.permissions;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.medstudioinc.mulriplepermissionssdk.R;

import java.util.ArrayList;

public class PermissionsFragment extends Fragment implements PermissionsCallback, ActivityResultCallback {
    PermissionsManager permissionsManager;
    PermissionsHelper permissionsHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.permissionsManager = InitPermissions.getInstance(getActivity(), this);
        this.permissionsManager.addActivityResultCallback(this);

        this.permissionsHelper = new PermissionsHelper();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.permissions_fragment, container, false);


        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!permissionsManager.checkPermissions()){
            //permissionsManager.startPermsActivity(EasyPermissionsActivity.class);
        }

        view.findViewById(R.id.btn_checkpermission).setOnClickListener(e-> {
            startActivity(new Intent(getContext(), PermissionsActivity.class));
        });

        view.findViewById(R.id.btn_listpermission).setOnClickListener(e-> {
            PermissionsUtil.with(getActivity(), InitPermissions.getList());
        });

        view.findViewById(R.id.btn_permission).setOnClickListener(e-> {
            permissionsManager.requestPermissions(this);
        });



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Toast.makeText(getContext(), requestCode + "", Toast.LENGTH_SHORT).show();
        //this.permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PermissionsConstants.ACTIVITY_RESULT_REQUEST){
            this.permissionsManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onPermissionGranted(int requestCode, String[] perms) {
        Toast.makeText(getContext(), "Permission granted.", Toast.LENGTH_SHORT).show();
        if (this.permissionsManager.checkPermissions()){
            Toast.makeText(getContext(), "Permission granted.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions) {

    }

    @Override
    public void onPermissionDenied(int requestCode, String[] permissions) {
        if (!this.permissionsManager.checkPermissions()){
            Toast.makeText(getContext(), "Permission denied.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onNeverAskAgain(int requestCode, boolean isAgain) {
        //this.permissionsManager.addSharedPrefNeverAskAgain(isAgain);
        //Toast.makeText(getContext(), "Set to never ask again.", Toast.LENGTH_SHORT).show();
        new AlertDialog.Builder(getContext())
                .setMessage("set to never ask again")
                .setPositiveButton("Allow", (dialog, which) -> requestWriteExternalStoragePermission())
                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void onRemoveItemFromList(int requestCode, String[] permissions) {

    }

    @Override
    public void startSettingsForResult(Intent intent, int request) {
        startActivityForResult(intent, request);
    }

    @Override
    public void onAppSettingsCanceled() {
        if (!this.permissionsManager.checkPermissions()){
            Toast.makeText(getContext(), "Please Allow Permissions app if you give access to your info", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Thank you", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestWriteExternalStoragePermission(){

    }
}
