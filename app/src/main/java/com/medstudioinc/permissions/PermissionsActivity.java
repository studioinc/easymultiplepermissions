package com.medstudioinc.permissions;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.medstudioinc.mulriplepermissionssdk.R;

import java.util.ArrayList;

public class PermissionsActivity extends AppCompatActivity implements PermissionsCallback, ActivityResultCallback{
    PermissionsManager permissionsManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        this.permissionsManager = InitPermissions.getInstance(this, this);
        this.permissionsManager.addActivityResultCallback(this);

        findViewById(R.id.btnCheckPerms).setOnClickListener(e-> {
            PermissionsUtil.with(this, InitPermissions.getList());
            //permissionsManager.requestPermissions(this);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PermissionsConstants.INTENT_CODE){
            Toast.makeText(this, "All Permissions granted.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Toast.makeText(this, requestCode + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startSettingsForResult(Intent intent, int request) {

    }

    @Override
    public void onAppSettingsCanceled() {

    }

    @Override
    public void onPermissionGranted(int requestCode, String[] permissions) {

    }

    @Override
    public void onPartialPermissionGranted(int requestCode, ArrayList<String> grantedPermissions) {

    }

    @Override
    public void onPermissionDenied(int requestCode, String[] permissions) {

    }

    @Override
    public void onNeverAskAgain(int requestCode, boolean isAgain) {

    }

    @Override
    public void onRemoveItemFromList(int requestCode, String[] permissions) {

    }
}
